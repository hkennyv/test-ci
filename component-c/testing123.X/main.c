/*
 * Project: rmp_v2_pic
 * File: main.c
 * Author(s): Russell Okamura
 * Date Created: June 26, 2020
 * Description:
 *    This project will be programmed the the PIC32 MCU on the RMPv2 software to 
 *        monitor the connected battery cells via the BQ76PL455A TI battery 
 *        monitoring chip, as well as communicate with an external application 
 *        receiving commands and reporting data and errors to the user.  
 *        Additionally, perturbation circuitry and switch matrix control are 
 *        managed and handled by communication with the on board FPGA chip.  
 * 
 * Change History:
 * 
 */

// Import Necessary Microchip Header Files
#define _SUPPRESS_PLIB_WARNING 1
#include <xc.h>                       // Relevent Processor Header File  
#include <plib.h>                     // Peripheral Library Header File
#include <sys/attribs.h>              // System Header File necessary for ISR(s)
#include <string.h>

/*
 * Oscillator Configuration 
 * ------------------------
 * Using the Fast RC Oscillator with PLL.
 * See docs/oscillator.md
 *
 * System Clock = 80 Mhz
 * Peripheral Bus Clock = 40 Mhz
 */
#pragma config FNOSC    = FRCPLL        // Oscillator Selection - Fast RC Osc with PLL
#pragma config FPLLIDIV = DIV_2         // PLL Input Divider - Divide by 2
#pragma config FPLLMUL  = MUL_20        // PLL Multiplier - Multiply by 20
#pragma config FPLLODIV = DIV_1         // PLL Output Divider - Divide by 1


#pragma config FPBDIV   = DIV_2         // Peripheral Bus Clock Divider - Divide by 2

/*
 * Function: main
 * --------------
 * Call initialization and infinitely loop and wait for interrupts.
 */
int main(void){
  return 0;
}
