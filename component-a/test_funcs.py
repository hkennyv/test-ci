from funcs import add, mul


class TestFuncs:
    def test_add_1(self):
        x = 2
        y = 2
        expected = 4
        res = add(x, y)
        assert res == expected, f'expected {expected}, got {res}'

    def test_add_2(self):
        x = 3
        y = -1
        expected = 2
        res = add(x, y)
        assert res == expected, f'expected {expected}, got {res}'

    def test_mul_1(self):
        x = 3
        y = -1
        expected = -3
        res = mul(x, y)
        assert res == expected, f'expected {expected}, got {res}'

    def test_mul_2(self):
        x = -5
        y = -2
        expected = 10
        res = mul(x, y)
        assert res == expected, f'expected {expected}, got {res}'
